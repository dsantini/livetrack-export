<?php
require_once("utils.php");

interface IConverter
{
	/**
	 * @return string The extension of the file where the output will be printed 
	 */
	public function getExtension();

	/**
	 * @return string The mime type of the output
	 */
	public function getMimeType();

	/**
	 * @param mixed $data Data to convert
	 * @return string Content of the output
	 */
	public function convert($data);
}

class JsonConverter implements IConverter
{
	public function getExtension()
	{
		return "json";
	}

	public function getMimeType()
	{
		return "application/json";
	}

	public function convert($data)
	{
		if (empty($data))
			throw new InvalidArgumentException("Empty data received");

		if (!is_array($data))
			throw new InvalidArgumentException("Malformed data received");

		if (empty($data["trackPoints"]))
			throw new InvalidArgumentException("Data with no GPS points received");

		checkContent($data["trackPoints"]);

		return json_encode($data);
	}
}

class NotImplementedException extends BadMethodCallException
{
}

abstract class BaseConverter implements IConverter
{
	/**
	 * @var string $extension
	 */
	protected $extension;
	/**
	 * @var string $extension
	 */
	protected $mime;

	/**
	 * Actual converter (strategy pattern)
	 * 
	 * @param array<int,array{dateTime:string,position:array{lat:string,lon:string},altitude:string,fitnessPointData:array}> $data Data to convert
	 * @param string|false $activityType
	 * @param string|false $startTime
	 * @return string Content of the output
	 */
	protected abstract function concreteConvert($data, $activityType, $startTime);

	public function getExtension()
	{
		return $this->extension;
	}

	public function getMimeType()
	{
		return $this->mime;
	}

	public function convert($data)
	{
		if (empty($data))
			throw new InvalidArgumentException("Empty data received for conversion");

		if (!is_array($data))
			throw new InvalidArgumentException("Malformed data received for conversion");

		if (empty($data["trackPoints"]))
			throw new InvalidArgumentException("Data with no GPS points received for conversion");

		$dataArray = checkContent($data["trackPoints"]);

		$activityType = (empty($dataArray[0]["fitnessPointData"]["activityType"]) || !is_string($dataArray[0]["fitnessPointData"]["activityType"])) ?
			false : $dataArray[0]["fitnessPointData"]["activityType"];

		$startTime = $dataArray[0]["dateTime"];

		return $this->concreteConvert($dataArray, $activityType, $startTime);
	}
}

class StravaGpxConverter extends BaseConverter
{
	public function __construct()
	{
		$this->extension = "gpx";
		$this->mime = "application/gpx+xml";
	}

	protected function concreteConvert($data, $activityType, $startTime)
	{
		$ret = new SimpleXMLElement(
			'<?xml version="1.0" encoding="UTF-8"?>
			<gpx creator="LiveTrackExporter"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" 
				version="1.1" 
				xmlns="http://www.topografix.com/GPX/1/1" 
				xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
				xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
				<metadata>
					<link href="https://www.dsantini.it/livetrack/">
						<text>LiveTrack Exporter</text>
					</link>
				</metadata>
				<trk>
					<trkseg>
					</trkseg>
				</trk>
			</gpx>'
		); // https://www.php.net/manual/en/simplexml.examples-basic.php

		assert($ret->metadata instanceof SimpleXMLElement);
		assert($ret->trk instanceof SimpleXMLElement);
		assert($ret->trk->trkseg instanceof SimpleXMLElement);

		$ret->metadata->addChild("time", date('c'));
		$ret->trk->addChild("type", $activityType ? $activityType : "cycling");
		$ret->trk->addChild("name", $startTime ? $startTime : "Track");

		foreach ($data as $n => $punto) {
			if (!empty($punto["position"]["lat"]) && !empty($punto["position"]["lon"])) {
				$trkpt = $ret->trk->trkseg->addChild('trkpt');
				$trkpt->addAttribute("lat", $punto["position"]["lat"]);
				$trkpt->addAttribute("lon", $punto["position"]["lon"]);
				if (!empty($punto["altitude"]))
					$trkpt->addChild("ele", $punto["altitude"]);
				$trkpt->addChild("time", $punto["dateTime"]);
				$extensions = $trkpt->addChild("extensions");
				if (!empty($punto["fitnessPointData"]["powerWatts"]))
					$extensions->addChild("power", (string)$punto["fitnessPointData"]["powerWatts"]);
				$ext = $extensions->addChild("gpxtpx:TrackPointExtension");
				if (!empty($punto["fitnessPointData"]["heartRateBeatsPerMin"]))
					$ext->addChild("gpxtpx:hr", (string)$punto["fitnessPointData"]["heartRateBeatsPerMin"]);
				if (!empty($punto["fitnessPointData"]["cadenceCyclesPerMin"]))
					$ext->addChild("gpxtpx:cad", (string)$punto["fitnessPointData"]["cadenceCyclesPerMin"]);
			}
		}

		$out = $ret->asXML();
		if ($out == FALSE)
			throw new Exception("An error occurred during the conversion to XML");
		return $out;
	}
}

class GarminGpxConverter extends BaseConverter
{
	public function __construct()
	{
		$this->extension = "gpx";
		$this->mime = "application/gpx+xml";
	}

	protected function concreteConvert($data, $activityType, $startTime)
	{
		$ret = new SimpleXMLElement(
			'<?xml version="1.0" encoding="UTF-8"?>
			<gpx creator="LiveTrackExporter" version="1.0"
				xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
				xmlns:ns3="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
				xmlns="http://www.topografix.com/GPX/1/1"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
				<metadata>
					<link href="https://www.dsantini.it/livetrack/">
						<text>LiveTrack Exporter</text>
					</link>
				</metadata>
				<trk>
					<trkseg>
					</trkseg>
				</trk>
			</gpx>'
		); // https://www.php.net/manual/en/simplexml.examples-basic.php

		assert($ret->metadata instanceof SimpleXMLElement);
		assert($ret->trk instanceof SimpleXMLElement);
		assert($ret->trk->trkseg instanceof SimpleXMLElement);

		$ret->metadata->addChild("time", date('c'));
		$ret->trk->addChild("type", $activityType ? $activityType : "cycling");
		$ret->trk->addChild("name", $startTime ? $startTime : "Track");

		foreach ($data as $n => $punto) {
			if (!empty($punto["position"]["lat"]) && !empty($punto["position"]["lon"])) {
				$trkpt = $ret->trk->trkseg->addChild('trkpt');
				$trkpt->addAttribute("lat", $punto["position"]["lat"]);
				$trkpt->addAttribute("lon", $punto["position"]["lon"]);
				if (!empty($punto["altitude"]))
					$trkpt->addChild("ele", $punto["altitude"]);
				$trkpt->addChild("time", $punto["dateTime"]);
				$exts = $trkpt->addChild("extensions");
				$ext = $exts->addChild("ns3:TrackPointExtension");
				if (!empty($punto["fitnessPointData"]["heartRateBeatsPerMin"]))
					$ext->addChild("ns3:hr", (string)$punto["fitnessPointData"]["heartRateBeatsPerMin"]);
				if (!empty($punto["fitnessPointData"]["cadenceCyclesPerMin"]))
					$ext->addChild("ns3:cad", (string)$punto["fitnessPointData"]["cadenceCyclesPerMin"]);
			}
		}

		$out = $ret->asXML();
		if ($out == FALSE)
			throw new Exception("An error occurred during the conversion to XML");
		return $out;
	}
}

class TcxConverter extends BaseConverter
{
	public function __construct()
	{
		$this->extension = "tcx";
		$this->mime = "application/tcx+xml";
	}

	protected function concreteConvert($data, $activityType, $startTime)
	{
		$ret = new SimpleXMLElement(
			'<?xml version="1.0" encoding="UTF-8"?>
			<TrainingCenterDatabase
			xsi:schemaLocation="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd"
			xmlns:ns5="http://www.garmin.com/xmlschemas/ActivityGoals/v1"
			xmlns:ns3="http://www.garmin.com/xmlschemas/ActivityExtension/v2"
			xmlns:ns2="http://www.garmin.com/xmlschemas/UserProfile/v2"
			xmlns="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns4="http://www.garmin.com/xmlschemas/ProfileExtension/v1">
				<Activities>
					<Activity>
						<Lap>
							<Intensity>Active</Intensity>
							<TriggerMethod>Manual</TriggerMethod>
							<Track>
							</Track>
						</Lap>
					</Activity>
				</Activities>
				<Author xsi:type="Application_t">
					<Name>LiveTrackExporter</Name>
					<Build>
						<Version>
						<VersionMajor>1</VersionMajor>
						<VersionMinor>1</VersionMinor>
						<BuildMajor>0</BuildMajor>
						<BuildMinor>0</BuildMinor>
						</Version>
					</Build>
					<LangID>en</LangID>
				</Author>
			</TrainingCenterDatabase>'
		); // https://www.php.net/manual/en/simplexml.examples-basic.php

		assert($ret->Activities instanceof SimpleXMLElement);
		assert($ret->Activities->Activity instanceof SimpleXMLElement);
		assert($ret->Activities->Activity->Lap instanceof SimpleXMLElement);
		assert($ret->Activities->Activity->Lap->Track instanceof SimpleXMLElement);
		assert($ret->Author instanceof SimpleXMLElement);

		// https://www8.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd
		if (!$activityType || in_array(strtolower($activityType), ["cycling", "bicycling", "biking"]))
			$activityTypeOk = "Biking";
		elseif (in_array(strtolower($activityType), ["running", "jogging"]))
			$activityTypeOk = "Running";
		else
			$activityTypeOk = "Other";
		$ret->Activities->Activity->addAttribute("Sport", $activityTypeOk);

		$ret->Activities->Activity->addChild("Id", $startTime ? $startTime : "Track");

		if ($startTime)
			$ret->Activities->Activity->Lap->addAttribute("StartTime", $startTime);

		foreach ($data as $n => $punto) {
			if (!empty($punto["position"]["lat"]) && !empty($punto["position"]["lon"])) {
				$Trackpoint = $ret->Activities->Activity->Lap->Track->addChild('Trackpoint');
				$Trackpoint->addChild("Time", $punto["dateTime"]);
				$Position = $Trackpoint->addChild("Position");
				$Position->addChild("LatitudeDegrees", $punto["position"]["lat"]);
				$Position->addChild("LongitudeDegrees", $punto["position"]["lon"]);
				if (!empty($punto["altitude"]))
					$Trackpoint->addChild("AltitudeMeters", $punto["altitude"]);
				if (!empty($punto["fitnessPointData"]["distanceMeters"]))
					$Trackpoint->addChild("DistanceMeters", (string)$punto["fitnessPointData"]["distanceMeters"]);
				if (!empty($punto["fitnessPointData"]["heartRateBeatsPerMin"])) {
					$HeartRateBpm = $Trackpoint->addChild("HeartRateBpm");
					$HeartRateBpm->addAttribute("xsi:type", "HeartRateInBeatsPerMinute_t");
					$HeartRateBpm->addChild("Value", (string)$punto["fitnessPointData"]["heartRateBeatsPerMin"]);
				}
				if (!empty($punto["fitnessPointData"]["cadenceCyclesPerMin"]))
					$Trackpoint->addChild("Cadence", (string)$punto["fitnessPointData"]["cadenceCyclesPerMin"]);
				$Extensions = $Trackpoint->addChild("Extensions");
				$TPX = $Extensions->addChild("ns3:TPX", null);
				if (!empty($punto["speed"]))
					$TPX->addChild("ns3:Speed", (string)$punto["speed"]);
				if (!empty($punto["fitnessPointData"]["powerWatts"]))
					$TPX->addChild("ns3:Watts", (string)$punto["fitnessPointData"]["powerWatts"]);
			}
		}

		$out = $ret->asXML();
		if ($out == FALSE)
			throw new Exception("An error occurred during the conversion to XML");
		return $out;
	}
}

class KmlConverter extends BaseConverter
{
	public function __construct()
	{
		$this->extension = "kml";
		$this->mime = "application/vnd.google-earth.kml+xml";
	}

	protected function concreteConvert($data, $activityType, $startTime)
	{
		$ret = new SimpleXMLElement(
			'<?xml version="1.0" encoding="UTF-8"?>
			<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
				<Placemark>
					<description>Created with LiveTrack exporter ( https://www.dsantini.it/livetrack/ )</description>
					<Style id="default">
						<LineStyle>
							<color>ff0055ff</color>
							<width>3</width>
						</LineStyle>
					</Style>
					<LineString>
						<coordinates>
						</coordinates>
					</LineString>
				</Placemark>
			</kml>'
		);

		assert($ret->Placemark instanceof SimpleXMLElement);
		assert($ret->Placemark->LineString instanceof SimpleXMLElement);
		assert($ret->Placemark->LineString->coordinates instanceof SimpleXMLElement);

		$ret->Placemark->addChild("name", $startTime ? $startTime : "Track");

		$ret->Placemark->LineString->coordinates[0] = implode(" ", array_map(
			function ($punto) {
				if (!empty($punto["position"]["lat"]) && !empty($punto["position"]["lon"]))
					return $punto["position"]["lon"] . "," . $punto["position"]["lat"];
				else
					return false;
			},
			$data
		));

		$out = $ret->asXML();
		if ($out == FALSE)
			throw new Exception("An error occurred during the conversion to XML");
		return $out;
	}
}

class CsvConverter extends BaseConverter
{
	public function __construct()
	{
		$this->extension = "csv";
		$this->mime = "text/csv";
	}

	protected function concreteConvert($data, $activityType, $startTime)
	{
		$tmpFile = tmpfile();
		fputcsv($tmpFile, ['Time', 'Latitude', 'Longitude', 'Altitude', 'Speed', 'Distance', 'Heart rate', 'Cadence', 'Power']);

		foreach ($data as $punto) {
			if (!empty($punto["position"]["lat"]) && !empty($punto["position"]["lon"])) {
				fputcsv($tmpFile, [
					$punto["dateTime"],
					$punto["position"]["lat"],
					$punto["position"]["lon"],
					empty($punto["altitude"]) ? null : $punto["altitude"],
					empty($punto["speed"]) ? null : $punto["speed"],
					empty($punto["fitnessPointData"]["distanceMeters"]) ? null : $punto["fitnessPointData"]["distanceMeters"],
					empty($punto["fitnessPointData"]["heartRateBeatsPerMin"]) ? null : $punto["fitnessPointData"]["heartRateBeatsPerMin"],
					empty($punto["fitnessPointData"]["cadenceCyclesPerMin"]) ? null : $punto["fitnessPointData"]["cadenceCyclesPerMin"],
					empty($punto["fitnessPointData"]["powerWatts"]) ? null : $punto["fitnessPointData"]["powerWatts"],
				]);
			}
		}

		rewind($tmpFile);
		$out = stream_get_contents($tmpFile);
		fclose($tmpFile);
		return $out;
	}
}

/*class FitConverter extends BaseConverter {
	public function __construct(){
		$this->extension = "fit";
		$this->mime = "application/octet-stream";
		throw new NotImplementedException(".fit conversion not yet implemented");
	}

	protected function concreteConvert($data, $activityType, $startTime){
		return "";
	}
}*/
