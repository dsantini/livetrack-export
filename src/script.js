const url_regex = /http[s]*:\/\/livetrack\.garmin\.com\/session\/([a-z\-0-9]+)\/token\/([A-Z0-9]+)/g;
let last_id = null,
    last_token = null,
    last_data = null;

/*document.getElementById("check_url_btn").addEventListener("click", checkURL);
document.getElementById("export_strava_gpx_btn").addEventListener("click", exportStravaGPX);
document.getElementById("export_garmin_gpx_btn").addEventListener("click", exportGarminGPX);
document.getElementById("export_tcx_btn").addEventListener("click", exportTCX);
document.getElementById("export_kml_btn").addEventListener("click", exportKML);

function checkURL() {
    let success = false;
    const validURL = fetchID();
    if (validURL) {
        success = fetchData(
            () => alert("An error occurred while contactng garmin servers"),
            null
        );
    } else {
        alert("Error: empty or invalid LiveTrack URL");
    }
    return success;
}

function exportStravaGPX() {
    if (checkURL())
        location.href = "export.php?format=strava_gpx&id=" + last_id + "&token=" + last_token;
}

function exportGarminGPX() {
    if (checkURL())
        location.href = "export.php?format=garmin_gpx&id=" + last_id + "&token=" + last_token;
}

function exportTCX() {
    if (checkURL())
        location.href = "export.php?format=tcx&id=" + last_id + "&token=" + last_token;
}

function exportKML() {
    if (checkURL())
        location.href = "export.php?format=kml&id=" + last_id + "&token=" + last_token;
}*/

function setLastID(id) {
    last_id = id;
    document.getElementById("lt_cod_input").value = last_id;
}

function setLastToken(token) {
    last_token = token;
    document.getElementById("lt_token_input").value = last_token;
}

function setLastData(data) {
    last_data = data;
    const count = (data && typeof data.length !== "undefined") ? data.length : null;
    document.getElementById("data_count_input").value = count;
}

function fetchID() {
    const url = document.getElementById("lt_url_input").value;
    let success = false;
    if (url) { // Non empty URL
        const match_result = [...url.matchAll(url_regex)]; // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/matchAll
        success = match_result && match_result[0] && match_result[0][1] && match_result[0][2]; // Valid URL
        const id = success ? match_result[0][1] : null,
            token = success ? match_result[0][2] : null;
        console.log({ AZ: "fetchID non empty", success, id, token, last_id, last_token, /*last_data,*/ match_result, url_regex, url });
        if (last_id !== id || last_token !== token) { // URL changed since last time
            setLastID(id);
            setLastToken(token);
            //setLastData(null); // Different data for different URL
        }
    } else { // Empty URL
        setLastID(null);
        setLastToken(null);
        //setLastData(null);
    }
    console.log({ AZ: "fetchID end", success, last_id, last_token, /*last_data*/ });
    return success;
}

function fetchData(errorCallback, successCallback) {
    const json_url = getJsonUrl(last_id, last_token),
        xhr = new XMLHttpRequest();
    let success = !!json_url;
    document.getElementById("lt_json_url_input").value = json_url;

    /*
    try{
    	xhr.onreadystatechange = function() {
    		if (this.readyState == 4){
    			if(this.status == 200){
    				setLastData(JSON.parse(this.responseText));
    				console.log({AZ:"xhr.onreadystatechange success", json_url, last_data, restponse: xhr.responseText});
    				if(successCallback)
    					successCallback();
    			} else {
    				console.error({AZ:"xhr.onreadystatechange error", json_url, http_code: this.status, response: xhr.responseText});
    				if(errorCallback)
    					errorCallback();
    			}
    		}
    	};
    	xhr.onerror = errorCallback;
    	xhr.open("GET", json_url, true);
    	xhr.send();
    } catch(e){
    	success = false;
    	console.error(e);
    	if(errorCallback)
    		errorCallback();
    }
    */

    console.log({ AZ: "fetchData end", success, json_url });
    return success;
}

function getJsonUrl(id, token) {
    let url = null;
    if (id && token)
        url = "https://livetrack.garmin.com/services/trackLog/" + id + "/token/" + token;
    console.log({ AZ: "getJsonUrl", url, id, token });
    return url;
}