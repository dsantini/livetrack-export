<?php

/**
 * @param mixed $dataArray
 * @return array<int,array{dateTime:string,position:array{lat:string,lon:string},altitude:string,fitnessPointData:array}>
 * @psalm-suppress MixedReturnTypeCoercion
 */
function checkContent($dataArray){
    if(empty($dataArray))
        throw new InvalidArgumentException("Empty data received for conversion");

    if(!is_array($dataArray))
        throw new InvalidArgumentException("Malformed data received for conversion");

    foreach($dataArray as $point) {
        if(empty($point) || !is_array($point))
            throw new InvalidArgumentException("A point is invalid");

        if(empty($point["dateTime"]) || !is_string($point["dateTime"]))
            throw new InvalidArgumentException("A point has no timestamp");

        if(empty($point["position"]) || !is_array($point["position"]))
            throw new InvalidArgumentException("A point has no position");
        /*if(empty($point["position"]["lat"]) || !is_numeric($point["position"]["lat"]))
            throw new InvalidArgumentException("A point has no latitude");
        if(empty($point["position"]["lon"]) || !is_numeric($point["position"]["lon"]))
            throw new InvalidArgumentException("A point has no longitude");*/

        if(!empty($point["altitude"]) && !is_numeric($point["altitude"]))
            throw new InvalidArgumentException("A point has an altitude which is not numeric");

        if(!empty($point["fitnessPointData"]) && !is_array($point["fitnessPointData"]))
            throw new InvalidArgumentException("A point has invalid fitness data");
    }

    return $dataArray;
}