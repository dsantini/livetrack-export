<?php
require_once("./tests/testUtils.php");
require_once("./src/ConverterFactory.php");
use PHPUnit\Framework\TestCase;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class ConverterTest extends TestCase {
    /**
     * @return array<string,array>
     */
    public function getGoodFormatAndData() {
        return cartesianProduct(getGoodFormats(), getGoodData());
    }

    /**
     * @return array<string,array>
     */
    public function getBadFormatAndData(){
        return array_merge(
            cartesianProduct(getBadFormats(), getBadData()),
            cartesianProduct(getBadFormats(), getGoodData()),
            cartesianProduct(getGoodFormats(), getBadData())
        );
    }

    /**
     * @param string $format
     * @param mixed $data
     * @return string
     */
    private function convertData($format, $data){
        $converter = ConverterFactory::get($format);

        $out = $converter->convert($data);
        $this->assertNotEmpty($out);
        $this->assertIsString($out);

        $extension = $converter->getExtension();
        $this->assertNotEmpty($extension);
        $this->assertIsString($extension);
        $this->assertMatchesRegularExpression('/^[\w\d]{2,4}$/', $extension);

        $mimetype = $converter->getMimeType();
        $this->assertNotEmpty($mimetype);
        $this->assertIsString($mimetype);
        $this->assertMatchesRegularExpression('/^[\w]+\/[\w\+\-\.]+$/', $mimetype);

        if(in_array($format, ['garmin_gpx','strava_gpx','tcx','kml'])) {
            //error_log("$format => xml");
            /* Not even official file do validate correctly
            $dom = new DOMDocument();
            $dom->loadXML($out);
            $this->assertTrue($dom->validate(), "Failed DTD validation");
            */
        } elseif ($format == 'json') {
            //error_log("$format => json");
            $this->assertNotEmpty(json_decode($out), "Failed JSON validation");
        } elseif ($format == "csv") {
            //error_log("$format => csv");
            $this->assertNotEmpty(str_getcsv($out), "Failed CSV validation");
        } else {
            //error_log("$format => ?");
        }

        return $out;
    }

    /**
     * @test
     * @dataProvider getGoodFormatAndData
     * @covers checkContent
     * @covers ConverterFactory::get
     * @covers BaseConverter::convert
     * @covers BaseConverter::getExtension
     * @covers BaseConverter::getMimeType
     * @covers GarminGpxConverter::__construct
     * @covers GarminGpxConverter::concreteConvert
     * @covers StravaGpxConverter::__construct
     * @covers StravaGpxConverter::concreteConvert
     * @covers TcxConverter::__construct
     * @covers TcxConverter::concreteConvert
     * @covers KmlConverter::__construct
     * @covers KmlConverter::concreteConvert
     * @covers CsvConverter::__construct
     * @covers CsvConverter::concreteConvert
     * @covers JsonConverter::convert
     * @covers JsonConverter::getExtension
     * @covers JsonConverter::getMimeType
     * @param string $format
     * @param mixed $data
     * @return void
     */
    public function convertGoodData($format, $data) {
        $this->assertNotEmpty($format);
        $this->assertIsString($format);
        
        $this->assertNotEmpty($data);
        $this->assertIsArray($data);

        $this->convertData($format, $data);
    }

        /**
     * @test
     * @dataProvider getBadFormatAndData
     * @covers checkContent
     * @covers ConverterFactory::get
     * @covers BaseConverter::convert
     * @covers GarminGpxConverter::__construct
     * @covers StravaGpxConverter::__construct
     * @covers TcxConverter::__construct
     * @covers KmlConverter::__construct
     * @covers CsvConverter::__construct
     * //NO covers FitConverter::__construct
     * //NO covers FitConverter::concreteConvert
     * @covers JsonConverter::convert
     * @param string $format
     * @param object $data
     * @return void
     */
    public function convertBadData($format, $data) {
        $this->expectException(Exception::class);
        $this->convertData($format, $data);
    }
}