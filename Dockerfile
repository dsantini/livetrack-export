# https://hub.docker.com/_/php
FROM php:8-apache-buster AS base
WORKDIR /var/www

COPY ./composer_install.sh ./composer_install.sh
RUN chmod +x ./composer_install.sh && ./composer_install.sh
COPY ./composer.json /var/www/composer.json

# https://docs.docker.com/develop/develop-images/multistage-build/
# https://docs.docker.com/engine/reference/commandline/build/
FROM base AS dev
# https://gist.github.com/ben-albon/3c33628662dcd4120bf4
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
# https://github.com/docker-library/docs/tree/master/php#how-to-install-more-php-extensions
RUN apt-get update && \
	apt-get install -y libzip-dev zip git && \
	rm -rf /var/lib/apt/lists/*
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
RUN docker-php-ext-install -j$(nproc) zip
RUN php composer.phar install

# https://blog.gitguardian.com/how-to-improve-your-docker-containers-security-cheat-sheet/
FROM base AS prod
RUN apt-get update && \
	apt-get install -y libzip-dev zip && \
	rm -rf /var/lib/apt/lists/*
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN docker-php-ext-install -j$(nproc) zip
RUN php composer.phar install --no-dev --no-scripts --no-plugins --optimize-autoloader && \
	rm composer.phar
USER www-data
COPY --chown=www-data:www-data ./src /var/www/html

# docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY
# docker build --target "dev" --tag livetrack-export:dev .
# docker build --target "prod" --tag livetrack-export:prod .
# docker push livetrack-export
# docker rm livetrack-export_instance
# docker run --name "livetrack-export_instance" --publish "80:80" livetrack-export:prod
