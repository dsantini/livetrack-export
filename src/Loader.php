<?php
require_once("utils.php");

interface ILoader {
	/**
	 * @return array The loaded data
	 */
	public function getData();

	/**
	 * @return string The name of the loaded data
	 */
	public function getName();
}

abstract class BaseLoader implements ILoader {
	/**
	 * @var array $data
	 */
	protected $data;
	/**
	 * @var string $name
	 */
	protected $name;

	public function getData(){
		return $this->data;
	}

	public function getName(){
		return $this->name;
	}
}

class FileUploadLoader extends BaseLoader {
	/**
	 * @param array $file HTTP request file element
	 * @param boolean $onlyUploadedFiles
	 */
	public function __construct($file, $onlyUploadedFiles=true){
		/**
		 * @psalm-suppress DocblockTypeContradiction
		 */
		if(empty($file) || !is_array($file))
			throw new InvalidArgumentException("Input object is not valid");

		if(isset($file["error"]) && $file["error"]==UPLOAD_ERR_NO_FILE)
			throw new InvalidArgumentException("No file was uploaded");

		if(isset($file["error"]) && $file["error"]!==UPLOAD_ERR_OK)
			throw new InvalidArgumentException("An error occurred during upload (".intval($file["error"]).")");

		if(empty($file["tmp_name"]) || !is_string($file["tmp_name"]))
			throw new InvalidArgumentException("Empty or invalid file path");
		$filePath = $file["tmp_name"];

		if(!file_exists($filePath))
			throw new InvalidArgumentException("Invalid file path");

		if($onlyUploadedFiles && !is_uploaded_file($filePath))
			throw new InvalidArgumentException("Illegal file path");

		if(empty($file["type"]))
			throw new InvalidArgumentException("No mime type provided");
		$provided_mime = $file["type"];
		assert(is_string($provided_mime));

		if($provided_mime != "application/json")
			throw new InvalidArgumentException("The provided file type is not valid");
		
		$detected_mime = mime_content_type($filePath);
		if($detected_mime != "text/plain" && $detected_mime != "application/json")
			throw new InvalidArgumentException("The detected file type is not valid");

		$data = json_decode(file_get_contents($filePath), true);

		$err = json_last_error();
		if(empty($data) || $err!==JSON_ERROR_NONE)
			throw new InvalidArgumentException("The file content is not valid JSON. json_last_error=$err");

		if(!is_array($data))
			throw new InvalidArgumentException("Unknown format received");

		checkContent($data["trackPoints"]);

		$this->data = $data;

		if(empty($file["name"]) || !is_string($file["name"]))
			throw new InvalidArgumentException("Empty or invalid file name received");

		//if(!str_ends_with($file["name"], '.json')) // PHP 8
		if(substr($file["name"], strlen($file["name"]) - 5) != '.json') // PHP 7 and lower
			throw new InvalidArgumentException("Empty or invalid file name received");

		$this->name = basename($file["name"], ".json");
	}
}

class IdTokenLoader extends BaseLoader {
	/**
	 * @param string $sessionID
	 * @param string $token
	 */
	public function __construct($sessionID, $token){
		if(!preg_match('/^[a-z0-9\-]+$/', $sessionID))
			throw new InvalidArgumentException("Invalid session ID");
		/**
		 * @psalm-taint-escape ssrf
		 */
		$json_url = "https://livetrack.garmin.com/services/session/$sessionID/trackpoints";

		if(!preg_match('/^[A-Z0-9]+$/', $token))
			throw new InvalidArgumentException("Invalid token");

		$this->name = $sessionID;

		$ch = curl_init();
		if($ch===false)
			throw new Exception("Error initializing curl");
		curl_setopt($ch, CURLOPT_URL, $json_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
		assert(is_int($httpCode));
		if($httpCode === 0)
			throw new Exception("Unable to reach $json_url (error: ".curl_error($ch).")");
		elseif($httpCode != 200)
			throw new Exception("The Garmin server responded with an error ($httpCode for $json_url)");
		curl_close($ch);

		if(empty($response))
			throw new Exception("The Garmin server responded with empty data");
		assert(is_string($response));

		$data = json_decode($response, true);

		$err = json_last_error();
		if(empty($data) || $err!==JSON_ERROR_NONE)
			throw new Exception("The data received from Garmin server is not valid JSON. json_last_error=$err");

		if(!is_array($data))
			throw new Exception("Unknown format received from Garmin servers");

		if(empty($data["trackPoints"]))
			throw new Exception("No trackpoints received from Garmin servers. The tracking session may have already ended.");
		
		checkContent($data["trackPoints"]);

		$this->data = $data;
	}
}

class UrlLoader extends IdTokenLoader {
	/**
	 * @param string $url Garmin Livetrack session URL ( https://livetrack.garmin.com/session/.../token/... )
	 */
	public function __construct($url){
		if(empty($url))
			throw new InvalidArgumentException("Empty url");

		if (preg_match("/^http[s]*:\/\/gar\.mn\/\w+$/", $url)) {
			//TODO Implement use of gar.mn shortened URLs
			// HTTP/1.1 301 Moved Permanently
			// Location: https://livetrack.garmin.com/session/...
			throw new InvalidArgumentException("Use of shortened URLs (gar.mn/...) is not yet implemented");
		}

		$matches = [];
		if(!preg_match("/^http[s]*:\/\/livetrack\.garmin\.com\/session\/([a-z0-9\-]+)\/token\/([A-Z0-9]+)/", $url, $matches)){
			//error_log("Invalid url: $url");
			throw new InvalidArgumentException("Invalid URL");
		}

		parent::__construct($matches[1], $matches[2]);
	}
}