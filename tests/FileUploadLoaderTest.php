<?php
require_once("./tests/testUtils.php");
require_once("./src/ConverterFactory.php");
require_once("./src/Loader.php");
use PHPUnit\Framework\TestCase;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class FileUploadLoaderTest extends TestCase {
    /**
     * @return array<string,array>
     */
    public function getGoodFiles() {
        return basicDataProvider(getGoodFiles());
    }

    /**
     * @return array<string,array>
     */
    public function getBadFiles() {
        return basicDataProvider(getBadFiles());
    }

    /**
     * @return array<string,array>
     */
    public function getGoodFormatsAndFiles() {
        return cartesianProduct(getGoodFormats(), getGoodFiles());
    }

    /**
     * @return array<string,array>
     */
    public function getBadFormatsAndFiles() {
        return array_merge(
            cartesianProduct(getBadFormats(), getBadFiles()),
            cartesianProduct(getGoodFormats(), getBadFiles()),
            cartesianProduct(getBadFormats(), getGoodFiles())
        );
    }

    /**
     * @param array $file
     * @return void
     */
    private function loadFile($file) {
        $loader = new FileUploadLoader($file, false);

        $data = $loader->getData();
        $this->assertNotEmpty($data);
        $this->assertIsArray($data);

        $name = $loader->getName();
        $this->assertNotEmpty($name);
        $this->assertIsString($name);
        $this->assertMatchesRegularExpression('/^[\w\d\s_\-]+$/', $name);
    }

    /**
     * @test
     * @dataProvider getGoodFiles
     * @covers checkContent
     * @covers FileUploadLoader::__construct
     * @covers FileUploadLoader::getData
     * @covers FileUploadLoader::getName
     * @param array $file
     * @return void
     */
    public function loadGoodFile($file) {
        //throw new Exception("file=".json_encode($file));
        $this->assertNotEmpty($file);
        $this->assertIsArray($file);
        $this->loadFile($file);
    }

    /**
     * @test
     * @dataProvider getBadFiles
     * @covers checkContent
     * @covers FileUploadLoader::__construct
     * @covers FileUploadLoader::getData
     * @covers FileUploadLoader::getName
     * @param array $file
     * @return void
     */
    public function loadBadFile($file) {
        $this->expectException(InvalidArgumentException::class);
        $this->loadFile($file);
    }

    /**
     * @test
     * @covers FileUploadLoader::__construct
     * @return void
     */
    public function loadNotUploadedFile(){
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Illegal file path");
        (new FileUploadLoader([
                "error"=>UPLOAD_ERR_OK, "tmp_name"=>__DIR__."/mock_bike_1.json", "type"=>"application/json", "name"=>"mock_bike_1.json"
            ], true))->getData(); 
    }

    /**
     * @param string $format
     * @param array $file
     * @return void
     */
    private function loadAndConvertFile($format, $file) {
        $loader = new FileUploadLoader($file, false);

        $data = $loader->getData();
        $this->assertNotEmpty($data);
        $this->assertIsArray($data);

        $name = $loader->getName();
        $this->assertNotEmpty($name);
        $this->assertIsString($name);
        $this->assertMatchesRegularExpression('/^[\w\d\s_\-]+$/', $name);

        $converter = ConverterFactory::get($format);

        $out = $converter->convert($data);
        $this->assertNotEmpty($out);
        $this->assertIsString($out);

        $extension = $converter->getExtension();
        $this->assertNotEmpty($extension);
        $this->assertIsString($extension);
        $this->assertMatchesRegularExpression('/^[\w\d]{2,4}$/', $extension);

        $mimetype = $converter->getMimeType();
        $this->assertNotEmpty($mimetype);
        $this->assertIsString($mimetype);
        $this->assertMatchesRegularExpression('/^[\w]+\/[\w\+\-\.]+$/', $mimetype);
    }

    /**
     * @test
     * @dataProvider getGoodFormatsAndFiles
     * @covers checkContent
     * @covers FileUploadLoader::__construct
     * @covers FileUploadLoader::getData
     * @covers FileUploadLoader::getName
     * @covers ConverterFactory::get
     * @covers BaseConverter::convert
     * @covers BaseConverter::getExtension
     * @covers BaseConverter::getMimeType
     * @covers GarminGpxConverter::__construct
     * @covers GarminGpxConverter::concreteConvert
     * @covers StravaGpxConverter::__construct
     * @covers StravaGpxConverter::concreteConvert
     * @covers TcxConverter::__construct
     * @covers TcxConverter::concreteConvert
     * @covers KmlConverter::__construct
     * @covers KmlConverter::concreteConvert
     * @covers CsvConverter::__construct
     * @covers CsvConverter::concreteConvert
     * //NO covers FitConverter::__construct
     * //NO covers FitConverter::concreteConvert
     * @covers JsonConverter::convert
     * @covers JsonConverter::getExtension
     * @covers JsonConverter::getMimeType
     * @param string $format
     * @param array $file
     * @return void
     */
    public function loadAndConvertGoodFile($format, $file) {
        $this->assertNotEmpty($format);
        $this->assertIsString($format);
        $this->assertNotEmpty($file);
        $this->assertIsArray($file);
        $this->loadAndConvertFile($format, $file);
    }

    /**
     * @test
     * @dataProvider getBadFormatsAndFiles
     * @covers checkContent
     * @covers FileUploadLoader::__construct
     * @covers FileUploadLoader::getData
     * @covers FileUploadLoader::getName
     * @covers ConverterFactory::get
     * @covers BaseConverter::convert
     * @covers BaseConverter::getExtension
     * @covers BaseConverter::getMimeType
     * @covers GarminGpxConverter::__construct
     * @covers StravaGpxConverter::__construct
     * @covers TcxConverter::__construct
     * @covers KmlConverter::__construct
     * @covers CsvConverter::__construct
     * //NO covers FitConverter::__construct
     * @param string $format
     * @param array $file
     * @return void
     */
    public function loadAndConvertBadFile($format, $file) {
        $this->expectException(InvalidArgumentException::class);
        $this->loadAndConvertFile($format, $file);
    }
}