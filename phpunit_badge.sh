#!/bin/bash

# https://medium.com/@iffi33/adding-custom-badges-to-gitlab-a9af8e3f3569
# https://gist.github.com/aafaque33/5db799d9d77619671d77b1f3eddc695f

grep 'Tests' phpunit.log.txt

tests=$( grep --only-matching --perl-regexp "\d+(?= tests)|(?<=Tests: )\d+" phpunit.log.txt || echo 0 )
failures=$( grep --only-matching --perl-regexp "(?<=Failures: )\d+" phpunit.log.txt || echo 0 )
skipped=$( grep --only-matching --perl-regexp "(?<=Skipped: )\d+" phpunit.log.txt || echo 0 )
risky=$( grep --only-matching --perl-regexp "(?<=Risky: )\d+" phpunit.log.txt || echo 0 )
success=$(( $tests - $failures - $risky - $skipped ))
text="$success/$tests"

if [[ "$failures" -gt "0" ]]
then color='red'
elif [[ "$risky" -gt "0" ]]
then color='orange'
elif [[ "$skipped" -gt "0" ]]
then color='yellow'
else color='green'
fi

anybadge -l 'tests' -v "$text" -f 'phpunit.svg' -c "$color"

echo "$tests tests"
echo "$failures failures"
echo "$risky risky"
echo "$skipped skipped"
echo "$success successful"
echo "$color color"