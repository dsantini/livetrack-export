<?php
require_once(__DIR__."/config.inc.php");

/**
 * @return void
 * @link https://docs.sentry.io/product/security-policy-reporting/
 * @link https://developers.google.com/tag-manager/web/csp
 * @link https://www.bounteous.com/insights/2017/07/20/using-google-analytics-and-google-tag-manager-content-security-policy/
 */
function preparaHTML() {
    preparePage();
    header("Content-Type: text/html; charset=UTF-8");

    $reportUri = "";
    if(!empty(SENTRY_JS_ENDPOINT)) {
        $reportUri = "report-uri " . SENTRY_JS_ENDPOINT . "; ";
    }

    $googleAnalyticsConnectSrcs = '';
    $googleAnalyticsScriptSrcs = '';
    if(!empty(GOOGLE_ANALYTICS_ID)) {
        $googleAnalyticsConnectSrcs = 'https://*.google-analytics.com https://stats.g.doubleclick.net https://analytics.google.com https://*.analytics.google.com/g/collect https://www.googletagmanager.com https://www.google.com/ads/ga-audiences https://www.google.it/ads/ga-audiences https://www.google.ru/ads/ga-audiences https://www.google.co.in/ads/ga-audiences https://www.google.no/ads/ga-audiences https://www.google.co.jp/ads/ga-audiences https://www.google.dk/ads/ga-audiences https://www.google.de/ads/ga-audiences https://www.google.be/ads/ga-audiences https://www.google.nl/ads/ga-audiences https://www.google.fr/ads/ga-audiences https://www.google.co.hk/ads/ga-audiences https://www.google.ch/ads/ga-audiences';
        $googleAnalyticsScriptSrcs = 'https://www.googletagmanager.com/gtag/js https://www.google-analytics.com';
    }

    header(
        "Content-Security-Policy: ".
            "upgrade-insecure-requests; ".
            "default-src 'self'; ".
            "font-src 'self' fonts.gstatic.com; ".
            "img-src 'self' https://www.paypalobjects.com $googleAnalyticsConnectSrcs ; " .
            "style-src 'self' https://fonts.googleapis.com; " .
            "script-src 'self' https://browser.sentry-cdn.com $googleAnalyticsScriptSrcs ; " .
            "frame-ancestors 'none'; " .
            "object-src 'none'; " .
            "connect-src 'self' https://*.ingest.sentry.io https://*.tiles.mapbox.com https://api.mapbox.com https://events.mapbox.com $googleAnalyticsConnectSrcs ; " .
            $reportUri
    );
}

/**
 * @return void
 */
function preparaJS() {
    preparePage();
    header("Content-Type: application/javascript; charset=UTF-8");
}

/**
 * @return void
 */
function preparePage() {
    ini_set("error_log", "/var/log/livetrack-export.log");
    // https://docs.sentry.io/platforms/php/
    // https://docs.sentry.io/platforms/php/configuration/options/
    if(!empty(SENTRY_PHP_DSN)) {
        require_once(__DIR__."/../vendor/autoload.php");
        Sentry\init([
            'dsn' => SENTRY_PHP_DSN,
            'environment' => SENTRY_PHP_ENVIRONMENT,
            'traces_sample_rate' => SENTRY_PHP_SAMPLE_RATE
        ]);
    }
    session_start();
}
