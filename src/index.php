<?php
require_once("./funcs.php");
preparaHTML();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="preload" as="script" type="application/javascript" href="https://www.googletagmanager.com/gtag/js?id=<?=GOOGLE_ANALYTICS_ID;?>">
    <link 
        rel="preload"
        as="script"
        type="application/javascript"
        href="https://browser.sentry-cdn.com/7.8.0/bundle.min.js"
        integrity="sha384-PVOy/EiuuBcf464HEXLzrIR872jZ4k78GhHPH9YhXiXR0/E/s1FTIOv1rlZ792HR"
        crossorigin="anonymous"
    />
    <link rel="preload" as="script" type="application/javascript" href="./script.php" />
    <link rel="preload" as="style" type="text/css" href="./style.css" />
    <link rel="preload" as="style" type="text/css" href="https://fonts.googleapis.com/css?family=Inconsolata" />
    <link rel="preload" as="style" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Sans" />
    <link rel="preload" as="style" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" />
    <link rel="preload" as="image" type="image/gif" href="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" />
    
    <?php if(!empty(GOOGLE_ANALYTICS_ID)) { ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?=GOOGLE_ANALYTICS_ID;?>"></script>
    <?php
    }

    if(!empty(SENTRY_JS_DSN)) {
    ?>
        <script
            src="https://browser.sentry-cdn.com/7.8.0/bundle.min.js"
            integrity="sha384-PVOy/EiuuBcf464HEXLzrIR872jZ4k78GhHPH9YhXiXR0/E/s1FTIOv1rlZ792HR"
            crossorigin="anonymous"
        ></script>
    <?php } ?>
    <script src="./script.php" type="application/javascript"></script>

    <title>LiveTrack export</title>
    <!--<script async defer src="./script.js"></script>-->
    <meta name="description" content="Unofficial Garmin LiveTrack free exporter tool. Export to TCX, GPX, KML, JSON.">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta property="og:type" content="profile" />
    <meta property="og:url" content="https://www.dsantini.it/livetrack/" />
    <meta property="og:title" content="LiveTrack export" />
    <meta property="og:description" content="Unofficial Garmin LiveTrack free exporter tool. Export to TCX, GPX, KML, JSON." />
    <meta property="og:locale" content="en_US" />
</head>

<body>
    <h1>LiveTrack export</h1>
    <p>Unofficial Garmin LiveTrack exporter</p>

<?php if(!empty($_SESSION['error']) && is_string($_SESSION['error'])) {
    echo '<div class="error_box">'.htmlspecialchars($_SESSION['error']).'</div>';
    unset($_SESSION['error']);
} ?>

    <div id="main">
    <form action="./export.php" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>Insert LiveTrack URL</legend>
            <input type="url" name="livetrack_url" id="lt_url_input" placeholder="https://livetrack.garmin.com/session/..." />
        </fieldset>
        
        <fieldset id="json_fs">
            <legend>...OR alternatively choose a LiveTrack JSON file</legend>
            <input type="file" name="livetrack_file" id="lt_file_input" />
        </fieldset>

        <fieldset>
            <legend>Choose an export format</legend>
            <input type="radio" name="format" value="tcx" id="format_tcx" checked>
            <label for="format_tcx">TCX</label>
            <br />
            <input type="radio" name="format" value="garmin_gpx" id="format_garmin_gpx">
            <label for="format_garmin_gpx">GPX (Garmin style)</label>
            <br />
            <input type="radio" name="format" value="strava_gpx" id="format_strava_gpx">
            <label for="format_strava_gpx">GPX (Strava style)</label>
            <br />
            <input type="radio" name="format" value="kml" id="format_kml">
            <label for="format_kml">KML</label>
            <br />
            <input type="radio" name="format" value="csv" id="format_csv">
            <label for="format_csv">CSV</label>
            <br />
            <input type="radio" name="format" value="json" id="format_json">
            <label for="format_json">LiveTrack JSON</label>
        </fieldset>

        <input type="submit" value="Export">
    </form>
    </div>

    <hr />

    <div id="footer">
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="donate_form">
            <input type="hidden" name="cmd" value="_donations" />
            <input type="hidden" name="business" value="NA5HL6EM9LDJ6" />
            <input type="hidden" name="currency_code" value="EUR" />
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
            <!--<img alt="" border="0" src="https://www.paypal.com/en_IT/i/scr/pixel.gif" width="1" height="1" />-->
        </form>
        |
        <a href="https://gitlab.com/dsantini/livetrack-export/-/issues" title="Report a problem">Report a problem</a> |
        <a href="https://gitlab.com/dsantini/livetrack-export" title="Contribute to this project">Contribute</a> |
        <a href="https://www.dsantini.it/" title="Check out my website">About me</a>
    </div>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" />
    <link rel="stylesheet" href="./style.css" />
</body>

</html>