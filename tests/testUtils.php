<?php

/**
 * @return array<string,array>
 */
function getGoodFiles() {
    return [
        'File: mock_bike_1.json' => [
            "name" => "mock_bike_1.json", "tmp_name" => __DIR__."/mock_bike_1.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: mock_bike_2.json' => [
            "name" => "mock_bike_2.json", "tmp_name" => __DIR__."/mock_bike_2.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: mock_run.json' => [
            "name" => "mock_run.json", "tmp_name" => __DIR__."/mock_run.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: bad name' => [
            "name" => "../../../../mock_bike_1.json", "tmp_name" => __DIR__."/mock_bike_1.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ]
    ];
}

/**
 * @return array<string,mixed>
 */
function getBadFiles() {
    return [
        'File: ull' => null,
        'File: Empty  string' => "",
        'File: Bad array' => [ "foo", "bar" ],
        'File: Empty file name' => [ "tmp_name" => __DIR__."/mock_bike_1.json", "type" => "application/json", "error" => UPLOAD_ERR_OK ],
        'File: Empty file path' => [ "name" => "mock_bike_1.json", "type" => "application/json", "error" => UPLOAD_ERR_OK ],
        'File: Empty file type' => [ "mock_bike_1.json", "tmp_name" => __DIR__."/mock_bike_1.json", "error" => UPLOAD_ERR_OK ],
        'File: Error' => [ "error" => UPLOAD_ERR_NO_FILE ],
        'File: Bad name extension' => [
            "error" => UPLOAD_ERR_OK, "name" => "FileUploadLoaderTest.php", "tmp_name" => "./tests/mock_bike_1.json", "type" => "application/json"
        ],
        'File: Bad type' => [
            "error" => UPLOAD_ERR_OK, "name" => "mock_bike_1.json", "tmp_name" => "./tests/mock_bike_1.json", "type" => "text/css"
        ],
        'File: Wrong type' => [
            "error" => UPLOAD_ERR_OK, "name" => "mock_bike_1.json", "tmp_name" => "./tests/FileUploadLoaderTest.php", "type" => "application/json"
        ],
        'File: Inexistent file' => [
            "name" => "wololooo.json", "tmp_name" => __DIR__."/wololooo.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Bad json' => [
            "name" => "bad_json.json", "tmp_name" => __DIR__."/bad_json.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Not json' => [
            "name" => "not_json.json", "tmp_name" => __DIR__."/not_json.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Empty file' => [
            "name" => "empty.json", "tmp_name" => __DIR__."/empty.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Empty json array' => [
            "name" => "empty_array.json", "tmp_name" => __DIR__."/empty_array.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Empty json object' => [
            "name" => "empty_object.json", "tmp_name" => __DIR__."/empty_object.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Null trackpoint' => [
            "name" => "null_trackpoint.json", "tmp_name" => __DIR__."/null_trackpoint.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Bad trackpoint' => [
            "name" => "bad_trackpoint.json", "tmp_name" => __DIR__."/bad_trackpoint.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Bad trackpoints' => [
            "name" => "bad_trackpoints.json", "tmp_name" => __DIR__."/bad_trackpoints.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Bad altitude' => [
            "name" => "bad_altitude.json", "tmp_name" => __DIR__."/bad_altitude.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: Bad fitnessPointData' => [
            "name" => "bad_fitnessPointData.json", "tmp_name" => __DIR__."/bad_fitnessPointData.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: No dateTime' => [
            "name" => "no_datetime.json", "tmp_name" => __DIR__."/no_datetime.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ],
        'File: No position' => [
            "name" => "no_position.json", "tmp_name" => __DIR__."/no_position.json", "type" => "application/json", "error" => UPLOAD_ERR_OK
        ]
    ];
}

/**
 * @return array<string,string>
 */
function getGoodFormats() {
    return [
        'Format: garmin gpx' => 'garmin_gpx',
        'Format: strava gpx' => 'strava_gpx',
        'Format: tcx' => 'tcx',
        'Format: kml' => 'kml',
        'Format: csv' => 'csv',
        'Format: json' => 'json',
    ];
}

/**
 * @return array<string,mixed>
 */
function getBadFormats() {
    return [
        'Format: null' => null,
        'Format: foo string' => 'foo',
        'Format: fit' => 'fit',
    ];
}

/**
 * @return array<string,string>
 */
function getElapsedIDs() {
    return [
        'ID: elapsed' => 'b4ed1209-ac10-435e-a4f3-d894e6c34440',
        'ID: inexistent' => 'foo-123'
    ];
}

/**
 * @return array<string,string>
 */
function getElapsedTokens() {
    return [
        'Token: elapsed' => '39CE119C58F89089A2516E65175E859B',
        'Token: inexistent' => 'BAR456'
    ];
}

/**
 * @return array<string,string>
 */
function getElapsedUrls() {
    return [
        'URL: bike, elapsed' => 'https://livetrack.garmin.com/session/b4ed1209-ac10-435e-a4f3-d894e6c34440/token/39CE119C58F89089A2516E65175E859B',
        'URL: web, bad session' => 'https://livetrack.garmin.com/session/foo-123/token/BAR456'
    ];
}

/**
 * @return array<string,mixed>
 */
function getBadIDs() {
    return [
        'ID: null' => null,
        'ID: number' => 42,
        'ID: empty string' => "",
        'ID: malicious' => '../..',
        'ID: malicious encoded' => '..%2F..',
    ];
}

/**
 * @return array<string,mixed>
 */
function getBadTokens() {
    return [
        'Token: null' => null,
        'Token: number' => 42,
        'Token: empty string' => "",
        'Token: malicious' => '../..',
        'Token: malicious encoded' => '..%2F..',
    ];
}

/**
 * @return array<string,mixed>
 */
function getBadUrls() {
    return [
        'URL: null' => null,
        'URL: number' => 42,
        'URL: empty' => "",
        'URL: web, malicious session' => 'https://livetrack.garmin.com/session/../../token/39CE119C58F89089A2516E65175E859B',
        'URL: web, malicious token' => 'https://livetrack.garmin.com/session/b4ed1209-ac10-435e-a4f3-d894e6c34440/token/../..',
        'URL: web, malicious encoded session' => 'https://livetrack.garmin.com/session/..%2F../token/39CE119C58F89089A2516E65175E859B',
        'URL: web, malicious encoded token' => 'https://livetrack.garmin.com/session/b4ed1209-ac10-435e-a4f3-d894e6c34440/token/..%2F..',
        'URL: api' => 'https://livetrack.garmin.com/services/session/b4ed1209-ac10-435e-a4f3-d894e6c34440/trackpoints',
    ];
}

/**
 * @return array<string,mixed>
 */
function getGoodData() {
    return [
        "Data: mock_bike_1" => json_decode(file_get_contents(__DIR__."/mock_bike_1.json"), true),
        "Data: mock_bike_2" => json_decode(file_get_contents(__DIR__."/mock_bike_2.json"), true),
        "Data: mock_run" => json_decode(file_get_contents(__DIR__."/mock_run.json"), true)
    ];
}

/**
 * @return array<string,mixed>
 */
function getBadData() {
    return [
        "Data: Null" => null,
        "Data: Empty array" => [],
        "Data: foo string" => "foo",
        "Data: Only other object " => ["foo"=>"bar"],
        "Data: Null trackpoints" => ["trackPoints"=>null],
        "Data: Empty trackpoints" => ["trackPoints"=>[]],
        "Data: String trackpoints" => ["trackPoints"=>"foo"],
        "Data: Object trackpoints" => ["trackPoints"=>["dateTime"=>null]],
        "Data: Null dateTime" => ["trackPoints"=>[["dateTime"=>null]]],
        "Data: Empty string dateTime" => ["trackPoints"=>[["dateTime"=>""]]],
        "Data: No position" => ["trackPoints"=>[["dateTime"=>"foo"]]],
        "Data: Null position" => ["trackPoints"=>[["dateTime"=>"foo", "position"=>null]]],
        "Data: String position" => ["trackPoints"=>[["dateTime"=>"foo", "position"=>"foo"]]],
    ];
}

/**
 * @param array<string,mixed> $arr
 * @return array<string,array>
 */
function basicDataProvider($arr){
    $out = [];
    foreach($arr as $k => $v) {
        $out[$k] = [$v];
    }
    return $out;
}

/**
 * @param array<string,mixed> $a
 * @param array<string,mixed> $b
 * @param array<string,mixed>|false $c
 * @return array<string,array>
 */
function cartesianProduct($a, $b, $c=false){
    $out = [];
    foreach($a as $aKey => $aValue) {
        foreach($b as $bKey => $bValue) {
            if($c) {
                foreach($c as $cKey => $cValue)
                    $out["$aKey/$bKey/$cKey"] = [$aValue, $bValue, $cValue];
            } else {
                $out["$aKey/$bKey"] = [$aValue, $bValue];
            }
        }
    }
    return $out;
}
