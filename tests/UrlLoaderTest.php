<?php
require_once("./tests/testUtils.php");
require_once("./src/ConverterFactory.php");
require_once("./src/Loader.php");
use PHPUnit\Framework\TestCase;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class UrlLoaderTest extends TestCase {
    /**
     * @return array<string,array>
     */
    public function getElapsedUrls(){
        return basicDataProvider(getElapsedUrls());
    }

    /**
     * @return array<string,array>
     */
    public function getBadUrls(){
        return basicDataProvider(getBadUrls());
    }

    /**
     * @return array<string,array>
     */
    public function getElapsedFormatsAndUrls() {
        return cartesianProduct(getGoodFormats(), getElapsedUrls());
    }

    /**
     * @return array<string,array>
     */
    public function getBadFormatsAndUrls() {
        return array_merge(
            cartesianProduct(getGoodFormats(), getBadUrls()),
            cartesianProduct(getBadFormats(), getBadUrls())
        );
    }

    /**
     * @param string $url
     * @return array
     */
    function loadUrls($url) {
        $loader = new UrlLoader($url);

        $data = $loader->getData();
        $this->assertNotEmpty($data);
        $this->assertIsArray($data);

        $name = $loader->getName();
        $this->assertNotEmpty($name);
        $this->assertIsString($name);
        $this->assertMatchesRegularExpression('/^[\w\d\s_\-]+$/', $name);

        return $data;
    }

    /**
     * @test
     * @dataProvider getElapsedUrls
     * @dataProvider getBadUrls
     * @covers checkContent
     * @covers UrlLoader::__construct
     * @covers UrlLoader::getData
     * @covers UrlLoader::getName
     * @covers IdTokenLoader::__construct
     * @param string $url
     * @return void
     */
    public function loadBadUrls($url){
        $this->expectException(Exception::class);
        $this->loadUrls($url);
    }

    /**
     * @param string $format
     * @param string $url
     * @return void
     */
    private function loadAndConvertUrl($format, $url) {
        $loader = new UrlLoader($url);

        $data = $loader->getData();
        $this->assertNotEmpty($data);
        $this->assertIsArray($data);

        $name = $loader->getName();
        $this->assertNotEmpty($name);
        $this->assertIsString($name);
        $this->assertMatchesRegularExpression('/^[\w\d\s_\-]+$/', $name);

        $converter = ConverterFactory::get($format);

        $out = $converter->convert($data);
        $this->assertNotEmpty($out);
        $this->assertIsString($out);

        $extension = $converter->getExtension();
        $this->assertNotEmpty($extension);
        $this->assertIsString($extension);
        $this->assertMatchesRegularExpression('/^[\w\d]{2,4}$/', $extension);

        $mimetype = $converter->getMimeType();
        $this->assertNotEmpty($mimetype);
        $this->assertIsString($mimetype);
        $this->assertMatchesRegularExpression('/^[\w]+\/[\w\+\-\.]+$/', $mimetype);
    }

    /**
     * @test
     * @dataProvider getElapsedFormatsAndUrls
     * @dataProvider getBadFormatsAndUrls
     * @covers checkContent
     * @covers UrlLoader::__construct
     * @covers UrlLoader::getData
     * @covers UrlLoader::getName
     * @covers IdTokenLoader::__construct
     * @covers ConverterFactory::get
     * @covers BaseConverter::convert
     * @covers BaseConverter::getExtension
     * @covers BaseConverter::getMimeType
     * @covers GarminGpxConverter::__construct
     * @covers StravaGpxConverter::__construct
     * @covers TcxConverter::__construct
     * @covers KmlConverter::__construct
     * @covers CsvConverter::__construct
     * @covers JsonConverter::convert
     * @param string $format
     * @param string $url
     * @return void
     */
    public function loadAndConvertBadUrl($format, $url){
        $this->expectException(Exception::class);
        $this->loadAndConvertUrl($format, $url);
    }
}