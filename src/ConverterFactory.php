<?php
require_once(__DIR__.'/Converter.php');

class ConverterFactory
{
	/**
	 * @param string $format
	 * @return IConverter
	 */
	public static function get($format){
		switch($format){
			case 'garmin_gpx': $ret = new GarminGpxConverter(); break;
			case 'strava_gpx': $ret = new StravaGpxConverter(); break;
			case 'tcx': $ret = new TcxConverter(); break;
			case 'kml': $ret = new KmlConverter(); break;
			//case 'fit': $ret = new FitConverter(); break;
			case 'csv': $ret = new CsvConverter(); break;
			case 'json': $ret = new JsonConverter(); break;
			default: throw new InvalidArgumentException("Invalid format");
		}
		return $ret;
	}
}