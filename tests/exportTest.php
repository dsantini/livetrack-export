<?php
require_once("./tests/testUtils.php");
require_once("./src/export.php");
use PHPUnit\Framework\TestCase;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class exportTest extends TestCase {
    /**
     * @return array<string,array>
     */
    public function getElapsedUrls(){
        return basicDataProvider(getElapsedUrls());
    }

    /**
     * @return array<string,array>
     */
    public function getBadUrls(){
        return basicDataProvider(getBadUrls());
    }

    /**
     * @return array<string,array>
     */
    public function getBadFormatsAndUrls() {
        return array_merge(
            cartesianProduct(getBadFormats(), getBadUrls()),
            cartesianProduct(getGoodFormats(), getBadUrls())
        );
    }

    /**
     * @return array<string,array>
     */
    public function getBadFormatsIDsAndTokens() {
        return array_merge(
            cartesianProduct(getBadFormats(), getBadIDs(), getBadTokens()),
            cartesianProduct(getBadFormats(), getElapsedIDs(), getBadTokens()),
            cartesianProduct(getBadFormats(), getBadIDs(), getElapsedTokens()),
            cartesianProduct(getGoodFormats(), getBadIDs(), getBadTokens()),
            cartesianProduct(getGoodFormats(), getElapsedIDs(), getBadTokens()),
            cartesianProduct(getGoodFormats(), getBadIDs(), getElapsedTokens()),
        );
    }

    /**
     * @return array<string,array>
     */
    public function getGoodFormatsAndFiles() {
        return cartesianProduct(getGoodFormats(), getGoodFiles());
    }

    /**
     * @return array<string,array>
     */
    public function getBadFormatsAndFiles() {
        return array_merge(
            cartesianProduct(getBadFormats(), getBadFiles()),
            cartesianProduct(getGoodFormats(), getBadFiles()),
            cartesianProduct(getBadFormats(), getGoodFiles())
        );
    }
    
    /**
     * @param array<string,mixed> $request
     * @param array<string,mixed> $files
     * @return string
     */
    private function export($request, $files){
        ob_start();
        onRequest($request, $files, false);
        $out = ob_get_clean();
        $this->assertNotEmpty($out);
        $this->assertIsString($out);
        return $out;
    }

    /**
     * @test
     * @dataProvider getBadFormatsAndUrls
     * @covers checkContent
     * @covers UrlLoader::__construct
     * @covers UrlLoader::getData
     * @covers UrlLoader::getName
     * @covers IdTokenLoader::__construct
     * @covers ConverterFactory::get
     * @covers GarminGpxConverter::__construct
     * @covers StravaGpxConverter::__construct
     * @covers TcxConverter::__construct
     * @covers KmlConverter::__construct
     * @covers CsvConverter::__construct
     * @covers JsonConverter::convert
     * @covers onRequest
     * @param string $format
     * @param string $url
     * @return void
     */
    public function exportBadFormatsAndUrls($format,$url){
        $out = $this->export([ "format"=>$format, "livetrack_url"=>$url ], []);

        $code = http_response_code();
        $this->assertIsInt($code);
        $this->assertNotEmpty($code);
        $this->assertNotEquals(200, $code);
        //$this->assertEquals($code, 400);
        //$this->assertContains($code, [500, 400]);
    }

    /**
     * @test
     * @dataProvider getBadFormatsIDsAndTokens
     * @covers checkContent
     * @covers UrlLoader::__construct
     * @covers UrlLoader::getData
     * @covers UrlLoader::getName
     * @covers IdTokenLoader::__construct
     * @covers ConverterFactory::get
     * @covers GarminGpxConverter::__construct
     * @covers StravaGpxConverter::__construct
     * @covers TcxConverter::__construct
     * @covers KmlConverter::__construct
     * @covers CsvConverter::__construct
     * @covers JsonConverter::convert
     * @covers onRequest
     * @param string $format
     * @param string $url
     * @return void
     */
    public function exportBadFormatsIDsAndTokens($format, $id, $token){
        $out = $this->export([ "format"=>$format, "livetrack_id"=>$id, "livetrack_token"=>$token ], []);

        $code = http_response_code();
        $this->assertIsInt($code);
        $this->assertNotEmpty($code);
        $this->assertNotEquals(200, $code);
        //$this->assertEquals($code, 400);
        //$this->assertContains($code, [500, 400]);
    }

    /**
     * @test
     * @dataProvider getGoodFormatsAndFiles
     * @covers checkContent
     * @covers FileUploadLoader::__construct
     * @covers FileUploadLoader::getData
     * @covers FileUploadLoader::getName
     * @covers ConverterFactory::get
     * @covers BaseConverter::convert
     * @covers BaseConverter::getExtension
     * @covers BaseConverter::getMimeType
     * @covers GarminGpxConverter::__construct
     * @covers GarminGpxConverter::concreteConvert
     * @covers StravaGpxConverter::__construct
     * @covers StravaGpxConverter::concreteConvert
     * @covers TcxConverter::__construct
     * @covers TcxConverter::concreteConvert
     * @covers KmlConverter::__construct
     * @covers KmlConverter::concreteConvert
     * @covers CsvConverter::__construct
     * @covers CsvConverter::concreteConvert
     * //NO covers FitConverter::__construct
     * //NO covers FitConverter::concreteConvert
     * @covers JsonConverter::convert
     * @covers JsonConverter::getExtension
     * @covers JsonConverter::getMimeType
     * @covers onRequest
     * @param string $format
     * @param array $file
     * @return void
     */
    public function exportGoodFormatsAndFiles($format,$file){
        $this->assertNotEmpty($format);
        $this->assertIsString($format);
        $this->assertNotEmpty($file);
        $this->assertIsArray($file);
        $out = $this->export([ "format"=>$format ] , [ "livetrack_file"=>$file ]);

        $code = http_response_code();
        $this->assertIsInt($code);
        $this->assertNotEmpty($code);
        $this->assertEquals(200, $code);
    }

    /**
     * @test
     * @dataProvider getBadFormatsAndFiles
     * @covers checkContent
     * @covers FileUploadLoader::__construct
     * @covers FileUploadLoader::getData
     * @covers FileUploadLoader::getName
     * @covers ConverterFactory::get
     * @covers BaseConverter::convert
     * @covers BaseConverter::getExtension
     * @covers BaseConverter::getMimeType
     * @covers GarminGpxConverter::__construct
     * @covers StravaGpxConverter::__construct
     * @covers TcxConverter::__construct
     * @covers KmlConverter::__construct
     * @covers CsvConverter::__construct
     * //NO covers FitConverter::__construct
     * @covers JsonConverter::convert
     * @covers JsonConverter::getExtension
     * @covers JsonConverter::getMimeType
     * @covers onRequest
     * @param string $format
     * @param array $file
     * @return void
     */
    public function exportBadFormatsAndFiles($format, $file) {
        $out = $this->export([ "format"=>$format ], [ "livetrack_file"=>$file ]);

        $code = http_response_code();
        $this->assertIsInt($code);
        $this->assertNotEmpty($code);
        if ($format!="json") {
            $this->assertNotEquals(200, $code);
            //$this->assertContains($code, [500, 400]);
        }
    }
}
