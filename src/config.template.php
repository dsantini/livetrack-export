<?php

define('SENTRY_PHP_DSN', '');
define('SENTRY_PHP_ENVIRONMENT', '');
define('SENTRY_PHP_SAMPLE_RATE', 0.5);

define('SENTRY_JS_DSN', '');
define('SENTRY_JS_ENVIRONMENT', '');
define('SENTRY_JS_ENDPOINT', '');

define('GOOGLE_ANALYTICS_ID', '');
