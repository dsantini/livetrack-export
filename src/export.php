<?php
require_once(__DIR__."/funcs.php");
require_once(__DIR__."/ConverterFactory.php");
require_once(__DIR__."/Loader.php");
//error_reporting(E_ALL);
error_reporting(E_ERROR);
session_start();

/**
 * @param array $request
 * @param array $files
 * @param boolean $onlyUploadedFiles
 * @return void
 */
function onRequest($request=[], $files=[], $onlyUploadedFiles=true) {
    /**
     * @var ILoader $loader
     */
    $loader = null;

    /**
     * @var IConverter $Converter
     */
    $converter = null;

    try{
        if(!empty($request["livetrack_url"]) && is_string($request["livetrack_url"])) {
            $loader = new UrlLoader($request["livetrack_url"]);
        } elseif (
                !empty($request["livetrack_id"])
                && is_string($request["livetrack_id"])
                && !empty($request["livetrack_token"])
                && is_string($request["livetrack_token"])
            ) {
            $loader = new IdTokenLoader($request["livetrack_id"], $request["livetrack_token"]);
        } elseif (!empty($files["livetrack_file"]) && is_array($files["livetrack_file"])) {
            $loader = new FileUploadLoader($files["livetrack_file"], $onlyUploadedFiles);
        } else { 
            throw new InvalidArgumentException("No input source given");
        }

        if(empty($request["format"]) || !is_string($request["format"])){
            throw new InvalidArgumentException("No format given");
        } else {
            $converter = ConverterFactory::get($request["format"]);
        }

        /**
         * @psalm-taint-escape header
         */
        $mimetype = $converter->getMimeType();
        if(!preg_match('/^[\w]+\/[\w\+\-\.]+$/', $mimetype))
            throw new Exception("Invalid or illegal mime type");

        /**
         * @psalm-taint-escape header
         */
        $name = $loader->getName();
        if(!preg_match('/^[\w\d\s_\- \(\)\.]+$/', $name))
            throw new Exception("Invalid or illegal file name");
        
        /**
         * @psalm-taint-escape header
         */
        $extension = $converter->getExtension();
        if(!preg_match('/^[\w\d]{2,4}$/', $extension))
            throw new Exception("Invalid or illegal output extension");

        $data = $loader->getData();
        $output = $converter->convert($data);

        http_response_code(200);
        header("Content-Type: $mimetype", true);
        header("Content-Disposition: attachment; filename=\"$name.$extension\"", true);
        echo $output;
    } catch(InvalidArgumentException $e){
        //echo $e->getTraceAsString();
        http_response_code(302);
        header("Location: ./");
        echo('<html><body>Input error: '.htmlspecialchars($e->getMessage()).'<br/><a href="./">Go back</a></body></html>');
        $_SESSION["error"] = "Input error: ".$e->getMessage();
        Sentry\captureException($e);
    } catch(Throwable $e){
        //error_log($e->getMessage());
        //error_log($e->getTraceAsString());
        http_response_code(302);
        header("Location: ./");
        echo('<html><body>Internal error: '.htmlspecialchars($e->getMessage()).'<br/><a href="./">Go back</a></body></html>');
        $_SESSION["error"] = "Internal error: ".$e->getMessage();
        Sentry\captureException($e);
    }
}

if(!empty($_REQUEST) || !empty($_FILES))
    onRequest($_REQUEST, $_FILES, true);