<?php
require_once("./funcs.php");
preparaJS();

if(!empty(GOOGLE_ANALYTICS_ID)) {
?>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', '<?=GOOGLE_ANALYTICS_ID;?>');
<?php
}
if(!empty(SENTRY_JS_DSN)) {
?>
Sentry.init({
    dsn: "<?=SENTRY_JS_DSN;?>",
    environment: "<?=SENTRY_JS_ENVIRONMENT;?>",
});
<?php
}
?>
