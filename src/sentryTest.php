<?php
require_once("./funcs.php");

// https://docs.sentry.io/platforms/php/enriching-events/identify-user/
Sentry\configureScope(function (Sentry\State\Scope $scope) {
    $scope->setUser(['email' => 'pippo.pluto@paperino.com']);
});

?>

<html>
<head>
    <script
        src="https://browser.sentry-cdn.com/6.1.0/bundle.min.js"
        type="application/javascript"
        integrity="sha384-T4wn6EUhrkGRYp9a0X2/uXu6frHKrfbSeO4zRRA4KIgrEaJMMRbpunhBtNahdsxW"
        crossorigin="anonymous"
    ></script>
    <script>
        // https://docs.sentry.io/platforms/javascript/
        // https://docs.sentry.io/platforms/javascript/configuration/options/
        // https://docs.sentry.io/platforms/javascript/install/cdn/
        Sentry.init({
            dsn: "<?=SENTRY_JS_DSN;?>",
            environment: "<?=SENTRY_JS_ENVIRONMENT;?>",
        });

        // https://docs.sentry.io/platforms/javascript/enriching-events/identify-user/
        Sentry.setUser({ email: "pippo.pluto@paperino.com" });
    </script>
</head>
<body>
    Ciao mondo
    <script>
        throw new Error("Pippo pluto paperino");
    </script>
</body>
</html>

<?php

//Sentry\captureException(new Exception("Pippo pluto paperino"));
throw new Exception("Pippo pluto paperino");
